import React, { Fragment, useEffect } from 'react';
import Navbar from './Navbar';
import Sidebar from './Sidebar';
import Footer from './Footer';
const Layout = ({ children }) => {
  return (
    <Fragment>
      <div className="wrapper">
        <Navbar />
        <Sidebar />
        {children}
        <Footer />
      </div>
    </Fragment>
  );
};

export default Layout;
