import $ from 'jquery';
import { useEffect } from 'react';

const CustomerChart2 = ({ data }) => {
    useEffect(() => {
        $(function () {
            //  Tạo một mảng rỗng, và các biến đếm
            let vDataCustomer = [];
            let vCountCustomerFromFrance = 0;
            let vCountCustomerFromUSA = 0;
            let vCountCustomerFromSNG = 0;
            let vCountCustomerFromSPAIN = 0;

            // import thuw vien chartJS
            $('body #scripts-hook-after').after(
                $('<script />', {
                    type: 'text/javascript',
                    class: 'chartJs',
                    src: '/plugins/chart.js/Chart.min.js',
                }),
            );

            const handleDataFromServer = () => {
                if (data !== null) {
                    // duyệt mảng data customer và tăng biến đếm khi tìm thấy khách hàng đến từ
                    // các quốc gia france, usa, singapore, spain
                    data.map((bI) => {
                        if (bI.country === 'France') {
                            vCountCustomerFromFrance++;
                        } else if (bI.country === 'USA') {
                            vCountCustomerFromUSA++;
                        } else if (bI.country === 'Singapore') {
                            vCountCustomerFromSNG++;
                        } else if (bI.country === 'Spain') {
                            vCountCustomerFromSPAIN++;
                        } else {
                            return bI;
                        }
                    });

                    // thêm vào mảng rỗng ở trên các dữ liệu phù hợp
                    vDataCustomer.push(vCountCustomerFromFrance);
                    vDataCustomer.push(vCountCustomerFromUSA);
                    vDataCustomer.push(vCountCustomerFromSNG);
                    vDataCustomer.push(vCountCustomerFromSPAIN);
                    // console.log(vDataCustomer);
                } else {
                    $('.card-body.donut').empty();
                    $('.card-body.donut').append(
                        $('<div/>', {
                            text: `Có vấn đề về kết nối mạng!!!`,
                            class: 'd-flex align-items-center justify-content-center',
                            style: 'height: 300px; font-size: 20px',
                        }),
                    );
                    return;
                }
            };

            handleDataFromServer();

            try {
                // tạo donut chart với dữ liệu đã cho
                let donutChartCanvas = $('#donutChart').get(0).getContext('2d');
                let donutData = {
                    labels: ['USA', 'France', 'Singapore', 'Spain'],
                    datasets: [
                        {
                            data: vDataCustomer,
                            backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef'],
                        },
                    ],
                };
                let donutOptions = {
                    maintainAspectRatio: false,
                    responsive: true,
                };

                new Chart(donutChartCanvas, {
                    type: 'doughnut',
                    data: donutData,
                    options: donutOptions,
                });
            } catch (error) {
                console.log(error);
            }
        });
    }, [data]);

    return (
        <div className="card">
            <div className="card-header border-0">
                <div className="d-flex justify-content-between">
                    <h3 className="card-title">Khách hàng nước ngoài</h3>
                    <small>(Đơn vị: người)</small>
                </div>
            </div>
            <div className="card-body donut">
                <canvas
                    id="donutChart"
                    style={{
                        minHeight: '300px',
                        height: '300px',
                        maxHeight: '300px',
                        maxWidth: '100%',
                    }}
                />
            </div>
        </div>
    );
};

export default CustomerChart2;
