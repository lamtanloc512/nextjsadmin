import $ from 'jquery';
import { useEffect } from 'react';

const CustomerChart = () => {
    // add dependencies jquery plugin
    useEffect(() => {
        if (typeof window !== 'undefined') {
            window.$ = window.jQuery = require('jquery');

            $('body').append(
                $('<script/>', {
                    src: '/plugins/flot/jquery.flot.js',
                    class: 'flotchart_pluginjs',
                    type: 'text/javascript',
                }),
            );
            $('body').append(
                $('<script/>', {
                    src: '/plugins/flot/plugins/jquery.flot.resize.js',
                    class: 'flotchart_pluginjs',
                    type: 'text/javascript',
                }),
            );
            $('body').append(
                $('<script/>', {
                    src: '/plugins/flot/plugins/jquery.flot.pie.js',
                    class: 'flotchart_pluginjs',
                    type: 'text/javascript',
                }),
            );

            // do function to draw chart
            $(function () {
                let bar_data = {
                    data: [
                        [1, 10],
                        [2, 8],
                        [3, 4],
                        [4, 13],
                        [5, 17],
                        [6, 9],
                        [7, 16],
                        [8, 13],
                        [9, 18],
                        [10, 12],
                        [11, 19],
                        [12, 20],
                    ],
                    bars: { show: true },
                };

                let options = {
                    grid: {
                        borderWidth: 1,
                        borderColor: '#f3f3f3',
                        tickColor: '#f3f3f3',
                    },
                    series: {
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: 'center',
                        },
                    },
                    colors: ['#3c8dbc'],
                    xaxis: {
                        axisLabel: '(Tháng)',
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 20,
                        axisLabelFontFamily: 'Arial',
                        ticks: [
                            [1, '1'],
                            [2, '2'],
                            [3, '3'],
                            [4, '4'],
                            [5, '5'],
                            [6, '6'],
                            [7, '7'],
                            [8, '8'],
                            [9, '9'],
                            [10, '10'],
                            [11, '11'],
                            [12, '12'],
                        ],
                    },
                    yaxis: {
                        axisLabel: 'Đơn vị (k)',
                        axisLabelUseCanvas: true,
                    },
                };
                $.plot('#bar-chart', [bar_data], options);
                /* END BAR CHART */
            });
        }
        return () => {
            $('body .flotchart_pluginjs').remove();
        };
    }, []);

    return (
        <div className="card">
            <div className="card-header border-0">
                <div className="d-flex justify-content-between">
                    <h3 className="card-title">Customers Visited</h3>
                    <a href="#">View Report</a>
                </div>
            </div>
            <div className="card-body">
                <div
                    id="bar-chart"
                    style={{ height: '300px', padding: '0px', position: 'relative' }}
                ></div>
            </div>
        </div>
    );
};

export default CustomerChart;
