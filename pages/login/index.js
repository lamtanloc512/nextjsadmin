import Script from 'next/script';
import { useEffect, useState } from 'react';
import $ from 'jquery';
import Head from 'next/head';
import Logo from './Logo';
import FormLogin from './FormLogin';
import LoginWithSocial from './LoginWithSocial';
import ForgotAndRegister from './ForgotAndRegister';

const Login = ({ isLogged }) => {
    // handle Sign in
    const handleSignIn = ({ email, password, remember }) => {
        if (email === 'lamtanloc512@gmail.com' && password === 'Lamtanloc321@') {
            isLogged(true);
        }
    };

    // add class style to body tag
    useEffect(() => {
        $('body').addClass('hold-transition login-page');
        return () => {
            $('body').removeClass('hold-transition login-page');
        };
    }, []);

    return (
        <>
            <div className="login-box">
                <Logo />
                <div className="card">
                    <div className="card-body login-card-body rounded">
                        <p className="login-box-msg">This App Written by LAMTANLOC</p>
                        <FormLogin onSubmit={handleSignIn} />
                        <LoginWithSocial />
                        <ForgotAndRegister />
                    </div>
                </div>
            </div>

            <Script id="scripts-hook-before" strategy="afterInteractive" />
            <Script id="scripts-hook-after" strategy="afterInteractive" />
            <Script id="scripts-hook-end" strategy="afterInteractive" />
        </>
    );
};

export default Login;
