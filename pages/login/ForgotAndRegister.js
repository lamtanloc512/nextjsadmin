import React from 'react';

const ForgotAndRegister = () => {
  return (
    <>
      <p className="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <p className="mb-0">
        <a href="register.html" className="text-center">
          Register a new membership
        </a>
      </p>
    </>
  );
};

export default ForgotAndRegister;
