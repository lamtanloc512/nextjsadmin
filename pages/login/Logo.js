import React from 'react';
import Link from 'next/link';

const Logo = () => {
    return (
        <div className="login-logo">
            <Link href="/">
                <a className="loginLogo">
                    <b>Next</b>ADMIN
                </a>
            </Link>
        </div>
    );
};

export default Logo;
