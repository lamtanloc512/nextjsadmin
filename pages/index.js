import Head from 'next/head';
import { Fragment } from 'react';
import { useEffect } from 'react';
import ContentHeader from './index/ContentHeader';
import CustomerChart from '../component/CustomerChart';
import CustomerChart2 from '../component/CustomerChart2';
import $ from 'jquery';

export default function Home({ customers }) {
    useEffect(() => {
        $('body').addClass('sidebar-mini');
        return () => {
            $('body').removeClass('sidebar-mini');
        };
    }, []);

    useEffect(() => {
        $(function () {
            $('body #scripts-hook-after').after(
                $('<script />', {
                    type: 'text/javascript',
                    class: 'dashboard',
                    src: '/dist/js/pages/dashboard3.js',
                }),
            );
        });
        return () => {
            $('body .chartJs').remove();
            $('body .dashboard').remove();
        };
    }, []);

    // console.log(customers);

    return (
        <Fragment>
            <div className="content-wrapper">
                <ContentHeader />
                <div className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="card">
                                    <div className="card-header border-0">
                                        <div className="d-flex justify-content-between">
                                            <h3 className="card-title">Online Store Visitors</h3>
                                            <a href="#">View Report</a>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="d-flex">
                                            <p className="d-flex flex-column">
                                                <span className="text-bold text-lg">820</span>
                                                <span>Visitors Over Time</span>
                                            </p>
                                            <p className="ml-auto d-flex flex-column text-right">
                                                <span className="text-success">
                                                    <i className="fas fa-arrow-up" /> 12.5%
                                                </span>
                                                <span className="text-muted">Since last week</span>
                                            </p>
                                        </div>
                                        <div className="position-relative mb-4">
                                            <canvas id="visitors-chart" height={200} />
                                        </div>
                                        <div className="d-flex flex-row justify-content-end">
                                            <span className="mr-2">
                                                <i className="fas fa-square text-primary" /> This
                                                Week
                                            </span>
                                            <span>
                                                <i className="fas fa-square text-gray" /> Last Week
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <CustomerChart />
                            </div>
                            <div className="col-lg-6">
                                <div className="card">
                                    <div className="card-header border-0">
                                        <div className="d-flex justify-content-between">
                                            <h3 className="card-title">Sales</h3>
                                            <a href="#">View Report</a>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="d-flex">
                                            <p className="d-flex flex-column">
                                                <span className="text-bold text-lg">
                                                    $18,230.00
                                                </span>
                                                <span>Sales Over Time</span>
                                            </p>
                                            <p className="ml-auto d-flex flex-column text-right">
                                                <span className="text-success">
                                                    <i className="fas fa-arrow-up" /> 33.1%
                                                </span>
                                                <span className="text-muted">Since last month</span>
                                            </p>
                                        </div>
                                        <div className="position-relative mb-4">
                                            <canvas id="sales-chart" height={200} />
                                        </div>
                                        <div className="d-flex flex-row justify-content-end">
                                            <span className="mr-2">
                                                <i className="fas fa-square text-primary" /> This
                                                year
                                            </span>
                                            <span>
                                                <i className="fas fa-square text-gray" /> Last year
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <CustomerChart2 data={customers} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export const getStaticProps = async () => {
    const jsdom = require('jsdom');
    let $ = require('jquery')(new jsdom.JSDOM().window);
    const getCustomerFromFUSS = await $.ajax({
        type: 'GET',
        url: `http://localhost:8080/customers/barchart`,
        success: function (response) {
            // console.log(response);
        },
        error: function (error) {
            // console.log(error);
        },
    });

    return {
        props: {
            customers: getCustomerFromFUSS,
        },
    };
};
