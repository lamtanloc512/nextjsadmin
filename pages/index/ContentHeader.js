import React from 'react';
import Link from 'next/link';
const ContentHeader = () => {
  return (
    <div className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1 className="m-0">Dashboard v3</h1>
          </div>
          <div className="col-sm-6">
            <ol className="breadcrumb float-sm-right">
              <li className="breadcrumb-item">
                <Link href="/">
                  <a>Home</a>
                </Link>
              </li>
              <li className="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentHeader;
