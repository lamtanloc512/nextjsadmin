import Layout from '../layout';
import '../styles/globals.css';
import '../styles/plugins/icheck-bootstrap/icheck-bootstrap.min.css';
import '../styles/plugins/select2/css/select2.min.css';
import '../styles/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css';
import '../styles/plugins/datatables-responsive/css/responsive.bootstrap4.min.css';
import '../styles/plugins/datatables-buttons/css/buttons.bootstrap4.min.css';
import '../styles/plugins/fontawesome-free/css/all.min.css';
import '../styles/css/adminlte.min.css';
import { Fragment, useEffect, useState } from 'react';
import Head from 'next/head';
import Script from 'next/script';
import Login from './login';
import { useRouter } from 'next/router';
import $ from 'jquery';

function MyApp({ Component, pageProps }) {
    const [isLoggedIn, setIsLoggedIn] = useState(true);
    const router = useRouter();

    useEffect(() => {
        if (typeof window !== 'undefined') {
            window.$ = window.jQuery = require('jquery');

            $('body #__NEXT_DATA__').before(
                $('<script />', {
                    src: '/plugins/jquery/jquery.min.js',
                    type: 'text/javascript',
                    class: 'jquery-themejs',
                }),
            );

            $('body #__NEXT_DATA__').before(
                $('<script />', {
                    src: '/plugins/bootstrap/js/bootstrap.bundle.min.js',
                    type: 'text/javascript',
                    class: 'bootstrap-themejs',
                }),
            );

            $(function () {
                const gPathJs = ['/dist/js/adminlte.min.js'];
                gPathJs.forEach((bI) => {
                    $('body').append(
                        $('<script />', {
                            type: 'text/javascript',
                            src: bI,
                            class: 'admin-themejs',
                        }),
                    );
                });
            });
        }

        return () => {
            $('body .jquery-themejs').remove();
            $('body .bootstrap-themejs').remove();
            $('body .admin-themejs').remove();
        };
    }, []);

    // useEffect(() => {
    //   if (!isLoggedIn) {
    //     route.push('/login');
    //   }
    // }, []);

    useEffect(() => {
        $(function () {
            if (isLoggedIn && router.pathname === '/login') {
                $('body').removeClass('hold-transition login-page');
                $('.login-box').css({ display: 'none' });
                $('.wrapper').fadeOut('fast');
                router.push('/');
                setTimeout(() => {
                    $('.wrapper').fadeIn('fast');
                }, 500);
            }
        });
        return () => {
            $('body').removeClass('hold-transition login-page');
        };
    }, [isLoggedIn, router]);

    // logged check
    const loginCheck = (paramCheck) => {
        console.log(paramCheck);
        if (paramCheck) {
            setIsLoggedIn(true);
        }
    };

    return (
        <Fragment>
            {isLoggedIn ? (
                <>
                    <Head>
                        <meta charSet="utf-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1" />
                        <title>AdminLTE</title>
                        <link rel="icon" href="/favicon.ico" key="favicon" />
                    </Head>
                    <Layout>
                        <Component {...pageProps} />
                    </Layout>
                </>
            ) : (
                <Login isLogged={loginCheck} />
            )}
            <Script id="scripts-hook-before" strategy="afterInteractive" />
            <Script id="scripts-hook-after" strategy="afterInteractive" />
            <Script id="scripts-hook-end" strategy="afterInteractive" />
        </Fragment>
    );
}

export default MyApp;
