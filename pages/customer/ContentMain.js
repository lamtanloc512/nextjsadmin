import React from 'react';
import DataTableComponent from './DataTableComponent';
import ReactLoading from 'react-loading';
import { useEffect, useState } from 'react';
import $ from 'jquery';
import JSZip from 'jszip';
import 'datatables.net-bs4';
import 'datatables.net-responsive-bs4';
import 'datatables.net-buttons-bs4';
import 'datatables.net-buttons/js/buttons.colVis';
import 'datatables.net-buttons/js/buttons.html5';
import 'datatables.net-buttons/js/buttons.flash';
import 'datatables.net-buttons/js/buttons.print';
import EditModal from './EditModal';
import DeleteModal from './DeleteModal';

const ContentMain = ({ data, deletecustomer }) => {
  if (typeof window !== 'undefined') {
    window.JSZip = JSZip;
  }

  const [loading, setLoading] = useState(true);

  //bootstrap modal state
  const [modalEditShow, setModalEditShow] = useState(false);
  const [modalDeleteShow, setModalDeleteShow] = useState(false);
  // declare data row state
  const [editModalData, setEditModalData] = useState({});
  const [deleteModalData, setDeleteModalData] = useState({});
  // id of table
  const gId = 'customer-table';

  useEffect(() => {
    //define column table
    const gColumn = [
      'id',
      'fullName',
      'phoneNumber',
      'address',
      'city',
      'state',
      'postalCode',
      'country',
      'salesRepEmployeeNumber',
      'creditLimit',
    ];

    let vDataTable = $(`#${gId}`).DataTable({
      responsive: true,
      lengthChange: false,
      autoWidth: false,
      dom: 'Bfrtip',
      buttons: ['excel', 'pdf', 'print', 'colvis'],
      columns: [
        { data: gColumn[0] },
        {
          data: null,
          render: function (row) {
            return row.firstName + ' ' + row.lastName;
          },
        },
        { data: gColumn[2] },
        { data: gColumn[3] },
        { data: gColumn[4] },
        { data: gColumn[5] },
        { data: gColumn[6] },
        { data: gColumn[7] },
        { data: gColumn[8] },
        { data: gColumn[9] },
        {
          data: null,
          render: function () {
            return `
                    <div class="btn-group d-flex justify-content-center" role="group">
                      <button class="btn-edit btn btn-sm btn-primary text-bold m-0 w-100">Edit</button>
                      <button class="btn-delete btn btn-sm btn-danger text-bold m-0 w-100">Delete</button>
                    </div>
                  `;
          },
        },
      ],
    });

    if (Object.keys(data).length > 0) {
      vDataTable.clear();
      vDataTable.rows.add(data);
      vDataTable.draw();
    }

    if (loading) {
      setTimeout(() => {
        setLoading(false);
      }, 500);
    }

    // add event for buttn edit
    $('.btn-edit').on('click', function () {
      let vTable = $(`#${gId}`).DataTable();
      let vRowDataEdit = vTable.row($(this).parents('tr')).data();
      setEditModalData(vRowDataEdit);
      setModalEditShow(true);
    });
    // add event for buttn delete
    $('.btn-delete').on('click', function () {
      let vTable = $(`#${gId}`).DataTable();
      let vRowData = vTable.row($(this).parents('tr')).data();
      setDeleteModalData(vRowData);
      setModalDeleteShow(true);
    });

    return () => {
      vDataTable.destroy();
      $(`.btn-edit`).off();
      $(`.btn-delete`).off();
    };
  }, [data, loading]);

  return (
    <section className="content">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header">
                {/* <h3 className="card-title">Customers Infomation</h3>
                 */}
                <button className="btn btn-success btn-addnew text-bold">
                  Add Customer
                </button>
              </div>
              <div className="card-body">
                {loading ? (
                  <div
                    className="d-block w-100 d-flex justify-content-center align-items-center"
                    style={{ height: '500px' }}
                  >
                    <ReactLoading
                      type={'spinningBubbles'}
                      color={'#000000'}
                      height={'100px'}
                      width={'100px'}
                    />
                  </div>
                ) : (
                  <>
                    <DataTableComponent
                      id={gId}
                      className={
                        'table table-bordered table-striped table-responsive-sm'
                      }
                    />
                    <EditModal
                      show={modalEditShow}
                      onHide={() => setModalEditShow(false)}
                      editmodaldata={editModalData}
                    />
                    <DeleteModal
                      show={modalDeleteShow}
                      onHide={() => setModalDeleteShow(false)}
                      deletemodaldata={deleteModalData}
                      deletecustomer={deletecustomer}
                    />
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContentMain;
