import React, { useEffect, useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import $ from 'jquery';

const EditModal = (props) => {
  const gModalData = props.editmodaldata;
  const [customerData, setCustomerData] = useState({});

  useEffect(() => {
    if (gModalData !== null) {
      setCustomerData({ ...gModalData });
    }
  }, [gModalData]);

  useEffect(() => {
    $('#__NEXT_DATA__').after(
      $('<script />', {
        type: 'text/javascript',
        src: '/plugins/select2/js/select2.full.min.js',
        class: 'select2js',
      })
    );
  }, []);

  useEffect(() => {
    $(function () {
      $('.select2').select2();
    });
  }, [gModalData]);

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="bg-primary text-white" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit customer
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="row">
          <div className="col">
            <div className="form-group">
              <label htmlFor="inputName">Firstname</label>
              <input
                type="text"
                id="input_firstname"
                className="form-control"
                defaultValue={customerData.firstName}
              />
            </div>
            <div className="form-group">
              <label htmlFor="inputName">Lastname</label>
              <input
                type="text"
                id="input_lastname"
                className="form-control"
                defaultValue={customerData.lastName}
              />
            </div>
            <div className="form-group">
              <label htmlFor="inputClientCompany">Telephone</label>
              <input
                type="text"
                id="input_telephone"
                className="form-control"
                defaultValue={customerData.phoneNumber}
              />
            </div>
            <div className="form-group">
              <label htmlFor="inputProjectLeader">Address</label>
              <input
                type="text"
                id="input_address"
                className="form-control"
                defaultValue={customerData.address}
              />
            </div>
          </div>
          <div className="col">
            <div className="form-group">
              <label htmlFor="inputName">City</label>
              <input
                type="text"
                id="input_city"
                className="form-control"
                defaultValue={customerData.city}
              />
            </div>
            <div className="form-group">
              <label htmlFor="inputStatus">State</label>
              <select
                id="input_state"
                className="form-control custom-select select2"
                defaultValue={customerData.state}
              >
                <option>Select one</option>
                <option>On Hold</option>
                <option>Canceled</option>
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="inputClientCompany">Credit limit</label>
              <input
                type="number"
                id="inputClientCompany"
                className="form-control"
                disabled={'disabled'}
                defaultValue={customerData.creditLimit}
              />
            </div>
            <div className="form-group">
              <label htmlFor="inputProjectLeader">Sale employees</label>
              <input
                type="number"
                id="inputProjectLeader"
                className="form-control"
                disabled={'disabled'}
                defaultValue={customerData.salesRepEmployeeNumber}
              />
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          className="text-bold"
          variant="primary"
          onClick={() => {
            console.log('click');
          }}
        >
          Save
        </Button>
        <Button className="text-bold" variant="dark" onClick={props.onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditModal;
