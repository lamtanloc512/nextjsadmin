import React from 'react';
import { Modal, Button } from 'react-bootstrap';
const DeleteModal = (props) => {
  const gModalData = props.deletemodaldata;
  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="bg-danger text-white" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Modal heading
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4 className="text-center">Are you sure to delete this customer?</h4>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="danger"
          className="btn_confirm_delete text-bold"
          onClick={() => props.deletecustomer(gModalData.id)}
        >
          Delete
        </Button>
        <Button
          variant="dark"
          className="btn_close text-bold"
          onClick={props.onHide}
        >
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DeleteModal;
