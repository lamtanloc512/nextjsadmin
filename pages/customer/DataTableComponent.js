const DataTableComponent = ({ id, className }) => {
  return (
    <>
      <table id={id} className={className}>
        <thead>
          <tr>
            <th>Stt</th>
            <th>Fullname</th>
            <th>Phone</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Postal Code</th>
            <th>Country</th>
            <th>Credit Limit</th>
            <th>Sale Employee</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
          <tr>
            <th>Stt</th>
            <th>Fullname</th>
            <th>Phone</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Postal Code</th>
            <th>Country</th>
            <th>Credit Limit</th>
            <th>Sale Employee</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </>
  );
};

export default DataTableComponent;
