import Head from 'next/head';
import { Fragment, useEffect, useState } from 'react';
import ContentHeader from './ContentHeader';
import ContentMain from './ContentMain';
import $ from 'jquery';

const Customer = ({ deleteCustomer, vData }) => {
  if (typeof window !== 'undefined') {
    window.jQuery = $;
  }
  const [state, setState] = useState([]);
  useEffect(() => {
    const gPathJs = [
      '/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js',
      '/plugins/datatables-responsive/js/responsive.bootstrap4.min.js',
      '/plugins/datatables-buttons/js/buttons.bootstrap4.min.js',
      '/plugins/jszip/jszip.min.js',
      '/plugins/pdfmake/pdfmake.min.js',
      '/plugins/pdfmake/vfs_fonts.js',
      '/plugins/datatables-buttons/js/buttons.html5.min.js',
      '/plugins/datatables-buttons/js/buttons.print.min.js',
      '/plugins/datatables-buttons/js/buttons.colVis.min.js',
    ];
    gPathJs.map((bI) => {
      $('#__NEXT_DATA__').after(
        $('<script />', {
          type: 'text/javascript',
          src: bI,
          class: 'datatable-plugin',
        })
      );
    });

    return () => {
      $('body .datatable-plugin').remove();
    };
  }, []);

  //setState equal data from gettServerSide
  useEffect(() => {
    const fetchCustomerState = async () => {
      if (Object.keys(vData).length > 0) {
        setTimeout(() => {
          setState(vData);
        }, 50);
      }
    };
    fetchCustomerState();
  }, [vData]);

  //delete customer
  function deleteCustomer(id) {
    // console.log('hello', id);
    setState(state.filter((element) => element.id !== id));
  }

  return (
    <Fragment>
      <Head>
        <meta name="description" content="Manage Customer" key="customer" />
      </Head>
      <div className="content-wrapper">
        <ContentHeader />
        <ContentMain data={state} deletecustomer={deleteCustomer} />
      </div>
    </Fragment>
  );
};

export async function getStaticProps() {
  const jsdom = require('jsdom');
  let $ = require('jquery')(new jsdom.JSDOM().window);

  const vData = await $.ajax({
    type: 'GET',
    url: 'http://localhost:8080/customers/',
    dataType: 'json',
    success: function (response) {
      return response;
    },
    error: function (err) {
      console.log(err);
    },
  });

  return {
    props: {
      vData,
    },
  };
}

export default Customer;
