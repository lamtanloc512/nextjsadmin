$(function () {
  $('#example1').DataTable().destroy();
  $('#example1').DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    dom: 'Bfrtip',
    buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
  });
  // .buttons()
  // .container()
  // .appendTo('#example1_wrapper .col-md-6:eq(0)');
});
